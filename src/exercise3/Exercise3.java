package exercise3;

import java.util.Random;

public class Exercise3 {
    public static void main(String[] args) {
        int N = 100000;
        int[] numbers = new int[N];

        for (int i = 0; i < N; i++) {
            int random = new Random().nextInt(100) + 1;
            numbers[i] = random;
        }

        long startTime = System.currentTimeMillis();

        numbers = bubbleSort(numbers);

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;

        for (int i = 0; i < numbers.length; i++) {
            if (i != numbers.length - 1) {
                System.out.print(numbers[i] + ", ");
            } else {
                System.out.println(numbers[i] + ".");
            }
        }

        System.out.println("Czas sortowania: " + elapsedTime);
    }

    public static int[] bubbleSort(int[] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int temp = a[j + 1];
                    a[j + 1] = a[j];
                    a[j] = temp;
                }
            }
        }
        return a;
    }
}
