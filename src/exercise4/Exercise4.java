package exercise4;

import java.util.Arrays;
import java.util.Random;

public class Exercise4 {
    public static void main(String[] args) {
        int N = 100000;
        int[] numbers = new int[N];

        for (int i = 0; i < N; i++) {
            int random = new Random().nextInt(100) + 1;
            numbers[i] = random;
        }

        long startTime = System.currentTimeMillis();

        Arrays.sort(numbers);

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;

        System.out.println(Arrays.toString(numbers));
        System.out.println("Czas sortowania: " + elapsedTime);
    }
}
