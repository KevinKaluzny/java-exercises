package exercise1;

public class Exercise1 {
    public static void main(String[] args) {
        int a = 2;
        int b = 5;

        a += b;
        b = a - b;
        a -= b;

        System.out.println("Liczba a: " + a);
        System.out.println("Liczba b: " + b);
    }
}
