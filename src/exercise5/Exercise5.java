package exercise5;

import org.omg.CORBA.WStringSeqHelper;

import java.util.Scanner;

public class Exercise5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wprowadź licznik:");
        int l = scan.nextInt();
        System.out.println("Wprowadź mianownik:");
        int m = scan.nextInt();

        try {
            System.out.println("Wynik dzielenia " + l + " / " + m + " = " + l / m);
        } catch (ArithmeticException exception) {
            System.out.println("Nie można podzielić przez zero!");
        }
    }
}
