package exercise7;

import exercise7.Animal.Animal;
import exercise7.Goldfish.Goldfish;
import exercise7.Human.Human;

public class Main {
    public static void main(String[] args) {
        Animal fish = new Goldfish();
        Animal human = new Human();

        System.out.println(fish);
        System.out.println(human);
    }
}
