package exercise6;

import exercise6.Addition.Addition;
import exercise6.Computation.Computation;
import exercise6.Multiplication.Multiplication;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        Computation computation;

        if (main.shouldMultiply()) {
            computation = new Multiplication(); // zaimplementuj brakującą klasę [DONE]
        }
        else {
            computation = new Addition(); // zaimplementuj brakującą klasę [DONE]
        }

        double argument1 = main.getArgument();
        double argument2 = main.getArgument();

        double result = computation.compute(argument1, argument2);
        System.out.println("Wynik: " + result);
    }

    private boolean shouldMultiply() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Mnożenie (true) czy dodawanie (false): ");
        return scan.nextBoolean(); // tutaj zapytaj użytkownika co chce zrobić (mnożenie czy dodawanie) [DONE]
    }

    private double getArgument() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbę: ");
        return scan.nextDouble(); // tutaj pobierz liczbę od użytkownika [DONE]
    }
}