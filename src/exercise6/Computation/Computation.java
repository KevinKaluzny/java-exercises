package exercise6.Computation;

public interface Computation {
    double compute(double argument1, double argument2);
}
