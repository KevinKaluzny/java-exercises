package exercise2;

public class Exercise2 {
    public static void main(String[] args) {
        int celsius = 100;
        float fahrenheit = parseToFahrenheit(celsius);

        System.out.println("Temperatura w stopniach Fahrenheita: " + fahrenheit);
    }
    static float parseToFahrenheit(float x) {
        return (float) (32 + 1.8 * x);
    }
}
